#include <Wire.h>
#include <LiquidCrystal.h>

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);


int PHOTO_RESISTOR_PIN = A3;
int LED_PIN = 11;

enum class MODE {
  IDLE,
  CALIBRATION,
  MEASUREMENT
};



// Measurement variables


#define MAX_LATENCIES_COUNT 20
int latencyIndex = 0;
unsigned long lastLatencies[MAX_LATENCIES_COUNT];
int minLatency = 999;
int avgLatency = 0;
int maxLatency = 0;
bool firstLoopCompleted = false;

// Calibration variables
enum class CALIBRATION_STEP {
  IDLE,
  LOW_POINT,
  HIGH_POINT,
  FAILED,
  COMPLETED
};

int CALIBRATION_TIME = 1000;
int CALIBRATION_DELAY = 500;
unsigned int CALIBRANTION_SAMPLES = 5000;

unsigned int lowPointSamples = 0;
unsigned int highPointSamples = 0;
double lowPointMax = 0;
double highPointMin = 9999;


bool ledState = false;
void setup() {
  Serial.begin(115200);
  pinMode(LED_PIN, OUTPUT);

  lcd.begin(16, 2);
  lcd.setCursor(0, 1);
  lcd.print("Latency tester");

  setCurrentMode(MODE::IDLE);

  analogWrite(10, 127);
}
char formatBuffer[16];

unsigned long previousMillis = 0;
unsigned long interval = 500;

int adc_key_in = 0;
#define btnNONE   -1
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4

int readLcdButtons() {
  int adc_key_in = analogRead(0);
  if (adc_key_in > 1000) return btnNONE;
  if (adc_key_in < 50)   return btnRIGHT;
  if (adc_key_in < 250)  return btnUP;
  if (adc_key_in < 450)  return btnDOWN;
  if (adc_key_in < 650)  return btnLEFT;
  if (adc_key_in < 850)  return btnSELECT;

  return btnNONE;
}

float readSensorVoltage() {
  return float(analogRead(PHOTO_RESISTOR_PIN)) * 5.0 / 1024.0;
}


bool currentLedState = LOW;
unsigned long lastSwitchTime = 0;
void setLed(bool state) {
  digitalWrite(LED_PIN, state);
  lastSwitchTime = millis();
  currentLedState = state;
}

MODE currentMode = MODE::IDLE;
CALIBRATION_STEP currentCalibrationStep = CALIBRATION_STEP::IDLE;


void printLatency() {
  sprintf(formatBuffer, "%3d   %3d   %4d", minLatency, avgLatency, maxLatency);
  lcd.setCursor(0, 1);
  lcd.print(formatBuffer);
}


void setCurrentMode(MODE newMode) {
  lcd.setCursor(0, 0);
  lcd.clear();
  switch (newMode) {
    case MODE::IDLE:
      lcd.print("IDLE");
      if (currentCalibrationStep == CALIBRATION_STEP::COMPLETED) {
        lcd.print(" (CAL)    ");
      }
      else if (currentCalibrationStep == CALIBRATION_STEP::FAILED) {
        lcd.print(" (N/CAL)");
      }
      break;
    case MODE::CALIBRATION:
      lcd.print("CAL");
      break;
    case MODE::MEASUREMENT:
      lcd.print("Min   Avg    Max");
      setLed(true);
      latencyIndex = 0;
      minLatency = 999;
      avgLatency = 0;
      maxLatency = 0;
      firstLoopCompleted = false;
      break;
  }
  currentMode = newMode;
  delay(500);
}


void loop() {
  unsigned long currentMillis = millis();
  bool updateScreen = false;

  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;
    updateScreen = true;
  }
  
  switch (currentMode) {
    case MODE::IDLE: {
      int btn = readLcdButtons();
      if (btn == btnLEFT) {
        setCurrentMode(MODE::CALIBRATION);
        currentCalibrationStep = CALIBRATION_STEP::IDLE;
      }
      else if (btn == btnSELECT) {
        setCurrentMode(MODE::MEASUREMENT);
      } 
      else if (btn == btnRIGHT) {
        setLed(!currentLedState);
        delay(200);
      }
      else if (updateScreen) {
        lcd.setCursor(12, 0);
        lcd.print(readSensorVoltage());
      
        if (avgLatency) {
          printLatency();
        }
      }
    }
    break;

    
    case MODE::MEASUREMENT: {
      int btn = readLcdButtons();
      if (btn == btnSELECT) {
        setCurrentMode(MODE::IDLE);
        setLed(LOW);
        return; 
      }
      double sensorVoltage = readSensorVoltage();

      if ((currentLedState && sensorVoltage >= highPointMin) || (!currentLedState && sensorVoltage <= lowPointMax)) {
        unsigned long latency = millis() - lastSwitchTime;
        // Toggle led at first so we will have spare time to print info
        setLed(!currentLedState);

        // Measure latency only on turning off
        if (!currentLedState) {
          lastLatencies[latencyIndex++] = latency;
          if (firstLoopCompleted) {
            if (latency < minLatency) {
              minLatency = latency;
            }
            if (latency > maxLatency) {
              maxLatency = latency;
            }
          }
        }
        
        if (latencyIndex == MAX_LATENCIES_COUNT) {
          double latenciesSum = 0;
          for (int i=0; i < MAX_LATENCIES_COUNT; i++) {
            latenciesSum += lastLatencies[i];
          }
          avgLatency = latenciesSum / MAX_LATENCIES_COUNT;
          
          latencyIndex = 0;

          printLatency();
        }
        firstLoopCompleted = true;
        
      }
      
    }
    break;

    case MODE::CALIBRATION: {
      switch (currentCalibrationStep) {
        case CALIBRATION_STEP::IDLE: {
          lowPointSamples = 0;
          highPointSamples = 0;
          lowPointMax = 0;
          highPointMin = 9999;
          currentCalibrationStep = CALIBRATION_STEP::LOW_POINT;
          Serial.println("goto = CALIBRATION_STEP::LOW_POINT");
            setLed(LOW);
        }
        break;

        case CALIBRATION_STEP::LOW_POINT: {
          double lowSensorVoltage = readSensorVoltage();
          if (lowSensorVoltage > lowPointMax) {
            lowPointMax = lowSensorVoltage;
          }
          lowPointSamples++;
          
          if (lowPointSamples > CALIBRANTION_SAMPLES) {
            setLed(HIGH);
            delay(CALIBRATION_DELAY);
            Serial.println("goto = CALIBRATION_STEP::HIGH_POINT");
            currentCalibrationStep = CALIBRATION_STEP::HIGH_POINT;
          }
        }
        break;

        case CALIBRATION_STEP::HIGH_POINT: {
          double highSensorVoltage = readSensorVoltage();
          if (highSensorVoltage < highPointMin) {
            highPointMin = highSensorVoltage;
          }
          highPointSamples++;
          if (highPointSamples > CALIBRANTION_SAMPLES) {
            setLed(LOW);
            lowPointMax += 0.3;
            highPointMin -= 0.3;
            Serial.print("lowPointMax = ");
            Serial.print(lowPointMax); 
            Serial.print(", highPointMin = ");
            Serial.println(highPointMin); 
            
            if (highPointMin > lowPointMax) {
              Serial.println("CALIBRATION_STEP::COMPLETED");
              currentCalibrationStep = CALIBRATION_STEP::COMPLETED;
            }
            else {
              Serial.println("CALIBRATION_STEP::FAILED");
              currentCalibrationStep = CALIBRATION_STEP::FAILED;
            }
            setCurrentMode(MODE::IDLE);
          }
        }
        break;
      }
    }
    break;
  }



  if (millis() - currentMillis > 3) {
    Serial.print("Loop threshold exceeded: ");
    Serial.print(millis() - currentMillis);
    Serial.println("ms");
  }
}
